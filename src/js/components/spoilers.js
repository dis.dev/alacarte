import  { hide, show, toggle } from 'slidetoggle';

export default function() {

    document.addEventListener('click', (event) => {
        if (event.target.closest('[data-spoiler-control]')) {
            const spoilerItem       = event.target.closest('[data-spoiler]');
            const spoilerGroup      = event.target.closest('[data-spoilers]');
            const spoilerContent    = spoilerItem.querySelector('[data-spoiler-content]');
            const spoilersArray     = spoilerGroup.querySelectorAll('[data-spoiler]');
            const isOneSpoiler      = spoilerGroup.hasAttribute('data-one-spoilers');
            const spoilerIsOpen     = spoilerItem.classList.contains('open');

            if (isOneSpoiler) {
                if (!spoilerIsOpen) {
                    spoilersArray.forEach(elem => {

                        if (elem.classList.contains('open')) {
                            hide(
                                elem.querySelector('[data-spoiler-content]'),
                                {
                                    miliseconds: 200,
                                }
                            )
                            elem.classList.remove('open')
                        }
                    });

                    show(
                        spoilerContent,
                        {
                            miliseconds: 200,
                        }
                    )
                    spoilerItem.classList.add('open')
                }
                else {

                }
            }
            else  {
                toggle(
                    spoilerContent,
                    {
                        miliseconds: 200,
                    }
                )
                spoilerItem.classList.toggle('open')
            }
        }
    })
}


